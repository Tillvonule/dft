from matplotlib import pyplot as  plt
import numpy as np
from scipy import fftpack
import ctypes
import time


# TODO:#
########
# DONE write converter 2D to 1D and back
# DONE include n as self.n_1d and self.n_2d
# include v_hartree in hamiltonian
# exchange corr pot


class DFT:
    def __init__(self, size):
        self.size = size
        self.wfkt = np.zeros(size*size)  # as 1D array
        self.hamiltonian = np.zeros((size*size, size*size))
        self.energy = None
        self.n_1d = None  # density function in 1D array

        # self.n_2d = None  # density function in 2D array
        self.hart_pot = np.zeros((size, size))
        self.xc_pot = np.zeros((size, size))

    # def differentiation(self, k, h):
    #     self.hamiltonian = np.zeros((self.size * self.size, self.size * self.size)) + 0j
    #     xc_pot = self.d2_to_d1(self.vxc())
    #     hart_pot = self.d2_to_d1(self.vhartree(h, k))
    #
    #     for i in range(self.size ** 2):
    #         for j in range(self.size ** 2):
    #             if i // self.size == 0:
    #                 if j // self.size == self.size - 1 and j % self.size == i % self.size:
    #                     self.hamiltonian[i][j] += 3 / (2 * k ** 2)
    #             elif i // self.size == self.size - 1:
    #                 if j // self.size == 0 and j % self.size == i % self.size:
    #                     self.hamiltonian[i][j] += 3 / (2 * k ** 2)
    #             elif i % self.size == 0:
    #                 if j % self.size == self.size - 1 and i // self.size == j // self.size:
    #                     self.hamiltonian[i][j] += 3 / (2 * h ** 2)
    #             elif i % self.size == self.size - 1:
    #                 if j % self.size == 0 and i // self.size == j // self.size:
    #                     self.hamiltonian[i][j] += 3 / (2 * h ** 2)
    #             if i == j:
    #                 self.hamiltonian[i][j] -= (49 / (18 * h ** 2) + 49 / (18 * k ** 2)) + xc_pot[i]*2 + hart_pot[i]*2
    #             elif i == j + 1 or i == j - 1:
    #                 self.hamiltonian[i][j] += 3 / (2 * h ** 2)
    #             elif i == j + self.size or i == j - self.size:
    #                 self.hamiltonian[i][j] += 3 / (2 * k ** 2)
    #             elif i == j + 2 or i == j - 2:
    #                 self.hamiltonian[i][j] -= 3 / (20 * h ** 2)
    #             elif i == j + 2 * self.size or i == j - 2 * self.size:
    #                 self.hamiltonian[i][j] -= 3 / (20 * k ** 2)
    #             elif i == j + 3 or i == j - 3:
    #                 self.hamiltonian[i][j] += 1 / (90 * h ** 2)
    #             elif i == j + 3 * self.size or i == j - 3 * self.size:
    #                 self.hamiltonian[i][j] += 1 / (90 * k ** 2)
    #
    #     self.hamiltonian *= -0.5

    def set_ham(self, k, h, box=True):
        '''
        inits the hamiltonian with nabla²/2 and the xc and hartree potential
        :param k:
        :param h:
        :param box:
        :return:
        '''

        vxc = self.d2_to_d1(self.vxc())
        vhart = self.d2_to_d1(self.vhartree(h, k))
        self.hamiltonian = np.zeros((self.size * self.size, self.size * self.size)) + 0j
        for i in range(self.size**2):
            for j in range(self.size**2):
                if not box:
                    if i//self.size == 0:
                        if j//self.size == self.size-1 and j%self.size == i%self.size:
                            self.hamiltonian[i][j] += 3 / (2 * k ** 2)
                    elif i//self.size == self.size-1:
                        if j//self.size == 0 and j%self.size == i%self.size:
                            self.hamiltonian[i][j] += 3 / (2 * k ** 2)
                    elif i%self.size == 0:
                        if j%self.size == self.size-1 and i//self.size == j // self.size:
                            self.hamiltonian[i][j] += 3 / (2 * h ** 2)
                    elif i%self.size == self.size-1:
                        if j%self.size == 0 and i//self.size == j // self.size:
                            self.hamiltonian[i][j] += 3 / (2 * h ** 2)
                if i == j:
                    self.hamiltonian[i][j] -= (49/(18*h**2) + 49/(18*k**2)) + vxc[i] + vhart[i]
                elif i == j + 1 or i == j - 1:
                    self.hamiltonian[i][j] += 3 /(2* h ** 2)
                elif i == j + self.size or i == j - self.size:
                        self.hamiltonian[i][j] += 3 /(2* k ** 2)
                elif i == j + 2 or i == j - 2:
                    self.hamiltonian[i][j] -= 3 / (20 * h ** 2)
                elif i == j + 2 * self.size or i == j - 2 * self.size:
                    self.hamiltonian[i][j] -= 3 / (20 * k ** 2)
                elif i == j + 3 or i == j - 3:
                    self.hamiltonian[i][j] += 1 / (90 * h ** 2)
                elif i == j + 3 * self.size or i == j - 3 * self.size:
                    self.hamiltonian[i][j] += 1 / (90 * k ** 2)

        self.hamiltonian *= -0.5
        return self.hamiltonian

    def periodic(self, k, h, wave):
        '''
        adds k dependency to hamiltonian with -i k nabla + k²/2
        :param k:
        :param h:
        :param wave:
        :return:
        '''
        self.set_ham(k, h, False)
        for i in range(self.size**2):
            for j in range(self.size**2):
                self.hamiltonian[i][j] += wave**2/2
                # 1 off
                if i == j + 1:
                    self.hamiltonian[i][j] += 3 / (4 * h) * (- 1j * wave)
                elif i == j - 1:
                    self.hamiltonian[i][j] -= 3 / (4 * h) * (- 1j * wave)
                elif i == j + 1*self.size:
                    self.hamiltonian[i][j] += 3 / (4 * k) * (- 1j * wave)
                elif i == j - 1*self.size:
                    self.hamiltonian[i][j] -= 3 / (4 * k) * (- 1j * wave)
                #  2 off
                elif i == j + 2:
                    self.hamiltonian[i][j] -= 3 / (20 * h) * (- 1j * wave)
                elif i == j - 2:
                    self.hamiltonian[i][j] += 3 / (20 * h) * (- 1j * wave)
                elif i == j + 2 * self.size:
                    self.hamiltonian[i][j] -= 3 / (20 * k) * (- 1j * wave)
                elif i == j - 2 * self.size:
                    self.hamiltonian[i][j] += 3 / (20 * k) * (- 1j * wave)
                # 3 off
                elif i == j + 3:
                    self.hamiltonian[i][j] += 1 / (60 * h) * (- 1j * wave)
                elif i == j - 3:
                    self.hamiltonian[i][j] -= 1 / (60 * h) * (- 1j * wave)
                elif i == j + 3 * self.size:
                    self.hamiltonian[i][j] += 1 / (60 * k) * (- 1j * wave)
                elif i == j - 3 * self.size:
                    self.hamiltonian[i][j] -= 1 / (60 * k) * (- 1j * wave)

    def solve(self):
        '''
        uses self.hamiltionian to calculate wavefunction and energy
        :return: wavefunction and energy
        '''
        self.energy, self.wfkt = np.linalg.eigh(self.hamiltonian)
        # self.energy, self.wfkt = sp.sparse.linalg.eigs(self.hamiltonian, k = 10)  # conjugated grandiance
        # 																			# k: amount of eigvecs and eigvals	
        return self.wfkt, self.energy

    # def wfkt_mat(self):
    #     '''
    #     redundant method to get 2d wavefunction. can just be done with d1_to_d2
    #     :return:
    #     '''
    #     wf_mat = [np.zeros((self.size, self.size))+0j for i in range(len(self.wfkt))]
    #     for n in range(len(self.wfkt)):
    #         for i in range(self.size):
    #             for j in range(self.size):
    #                 wf_mat[n][i][j] = self.wfkt[:, n][i+j]
    #     return wf_mat
    
    def n_init(self, h, k):
        '''
        this function sets the initial density function as a gaussian distribution
        :param h:
        :param k:
        :return: Nothing
        '''
        n_2d = np.zeros((self.size, self.size)) + 0j
        for i in range(self.size):  # work in 2D
            for j in range(self.size):
                x = h*(-0.5 * + i/self.size)
                y = k*(-0.5 * + i/self.size)
                n_2d[i][j] = 1/np.sqrt(np.pi) * np.exp(-(x + y)**2)  # gaussian for initial guess
        self.n_1d = self.d2_to_d1(n_2d)

    def d1_to_d2(self, d1):
        ret = np.zeros((self.size, self.size)) + 0j
        for i in range(self.size):
            for j in range(self.size):
                ret[i][j] = d1[i+j]
        return ret

    def d2_to_d1(self, d2):
        ret = np.zeros(self.size**2) + 0j
        for i in range(self.size):
            for j in range(self.size):
                ret[i+j] = d2[i][j]
        return ret

    def vhartree(self, h, k):
        '''
        Uses the 2D density function to calculate the hartree potential
        :param h:
        :param k:
        :return: 2D hartree potential, also updates self.hart_pot
        '''
        dens = fftpack.fft2(self.d1_to_d2(self.n_1d))  # forwards ft
        V = np.zeros((self.size, self.size)) + 0j
        for i in range(self.size):  # solving of transformed differantial equation
            for j in range(self.size):
                V[i][j] = 4. * np.pi * dens[i][j] / ((h/2 + i*h/self.size)**2 + (k/2 + i*k/self.size)**2)
        self.hart_pot = fftpack.ifft2(V)  # backwards ft
        return self.hart_pot
    
    def vxc(self):
        return self.xc_pot


if __name__ == "__main__":
    d = DFT(50)
    d.n_init(1, 1)

    for i in range(10):
        d.periodic(1, 1, 1)
        d.solve()
        en_arg = np.argsort(d.energy)
        d.n_1d = d.wfkt[en_arg[0]] * np.conj(d.wfkt[en_arg[0]])

    fig = plt.figure()
    ax = fig.add_subplot(111)
    cax = ax.matshow(np.real(d.d1_to_d2(d.wfkt[en_arg[0]])))
    # cax.set_clim(-0.3, 0.3)
    fig.colorbar(cax)

    plt.show()


